package ws

import (
	"crypto/tls"
	"fmt"

	"gitlab.com/gocord/ws/http"
)

/*
 *
 * A minimalistic WebSocket client implementation
 * made specficially for Discord API.
 *
 */

type Client struct {
	ln *tls.Conn
}

// best Go feature
func New() (_ Client) {
	return
}

func (c *Client) Connect() (err error) {
	// open tcp connection
	ln, err := tls.Dial("tcp", "gateway.discord.gg:443", &tls.Config{})
	if err != nil || ln == nil {
		return
	}
	fmt.Println("Connection opened successfully")
	c.ln = ln
	c.handshake()
	return
}

func (c *Client) Close() (err error) {
	c.ln.Close()
	return
}

// Go has http support but we will use our own implementation for a very simple upgrade request
func (c *Client) handshake() (err error) {
	message := http.Message{
		Method: "GET",
		Path:   "/?v=6&encoding=json",
		Headers: map[string]string{
			"Host":                  "gateway.discord.gg",
			"Upgrade":               "websocket",
			"Connection":            "Upgrade",
			"Sec-WebSocket-Key":     "Z29jb3JkMTMzNw==",
			"Sec-WebSocket-Version": "13",
		},
	}

	if _, err = c.ln.Write([]byte(message.Encode())); err != nil {
		return
	}
	buf := make([]byte, 1024)
	var n int
	if n, err = c.ln.Read(buf); err != nil {
		return
	}
	resp, err := http.NewResponse(string(buf[:n]))
	if err != nil {
		return
	}
	if resp.Status != 101 {
		return fmt.Errorf("Handshake failed")
	}
	return
}

func (c *Client) Read() {

}
