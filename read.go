package ws

import (
	"fmt"
	"io"
)

func readHeader(r io.Reader) (Header, error) {

	body := make([]byte, 12)

	h := Header{}

	if _, err := io.ReadFull(r, body); err != nil {
		return h, err
	}

	// FIN bit is the first bit (0x80 - 10000000)
	h.FIN = body[0]&0x80 != 0

	// RSV1-3 are the next 3 bits (0x70 - 01110000)
	// can store this in 4 bits so 01110000 becomes (0000)0111
	// TODO: check if this is ever used
	h.RSV = (body[0] & 0x70) >> 4

	// opcode is the last 4 bits (0x0F - 00001111)
	h.Opcode = body[0] & 0x0F

	// size we need to add to the buffer to read the payload
	var size int

	// masking bit is probably never used, but we'll check for now
	// TODO: check if used
	// masking is cringe i cba to learn how it works
	if body[0]&0x80 != 0 {
		h.Masked = true
		size += 4 // masking key takes 4 bytes
	}

	// payload length is the last 7 bits (0x7F - 01111111)
	payload_len := body[1] & 0x7F
	switch {
	case payload_len < 126:
		// h.Length is u64 as it can be up to 64 bits
		// it'll never be negative so we make it unsigned
		h.Length = uint64(payload_len)
	case payload_len == 126:
		size += 2 // 2 bytes for 16 bit length
	case payload_len == 127:
		size += 8 // 8 bytes for 64 bit length
	default:
		// invalid
		return h, fmt.Errorf("invalid payload length")
	}

	if size == 0 {
		// length is already set, can return header
		return h, nil
	}

	return h, nil
}
