package ws

type Header struct {
	FIN    bool
	RSV    byte
	Opcode byte
	Masked bool
	Length uint64
}

type Frame struct {
	Header,
	Payload []byte
}
