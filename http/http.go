package http

import (
	"fmt"
	"strconv"
	"strings"
)

type Message struct {
	Method  string
	Path    string
	Headers map[string]string
}

// high quality
func (m *Message) Encode() (s string) {
	s = m.Method + " " + m.Path + " HTTP/1.1" + "\r\n"
	for k, v := range m.Headers {
		s += k + ": " + v + "\r\n"
	}
	s += "\r\n"
	return
}

type Response struct {
	Status  uint16
	Headers map[string]string
}

func NewResponse(s string) (r Response, err error) {
	stat, err := strconv.Atoi(strings.Split(s, " ")[1])
	if err != nil {
		return
	}
	fmt.Println("Status:", stat)
	r.Status = uint16(stat)
	r.Headers = make(map[string]string)
	for _, line := range strings.Split(s, "\r\n") {
		if strings.Contains(line, ":") {
			kv := strings.Split(line, ":")
			r.Headers[kv[0]] = strings.TrimSpace(strings.Join(kv, ":"))
		}
	}
	return
}
